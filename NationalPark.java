import java.util.Scanner;

public class NationalPark{
	public static void main (String [] args){
		Dog[] pack = new Dog[4];
		Scanner sc = new Scanner(System.in);
		for(int i = 0; i<pack.length; i++){
			pack[i] = new Dog();
			System.out.println("Enter the name of the dog: ");
			pack[i].name = sc.next();
			System.out.println("Enter the breed of the dog: ");
			pack[i].breed = sc.next();
			System.out.println("Enter the number of legs the dog has: ");
			pack[i].numberOfLegs = sc.nextInt();
		};
		
		printStatement(pack);
		
		System.out.println(pack[0].canBark());
		
		System.out.println("Would you like to play fetch with " + pack[0].name + "? type y for yes, any button for no.");
		String answer = sc.next();
		boolean playAgain = true;
	
		if(answer.equals("y") ||answer.equals("Y")){
			while(playAgain){
				pack[0].canFetch();
				System.out.println("Would you like to play again? y/n?");
				String newAnswer = sc.next();
				if(newAnswer.equals("y") ||newAnswer.equals("Y")){
					playAgain = true;
				}else{
					playAgain = false;
				}
			}
		}
		System.out.println("Thanks for answering!");
	};
	
	public static void printStatement(Dog[] pack){
		System.out.println("");
		System.out.println("Your Last dog info: ");
		System.out.println("Name: " + pack[3].name);
		System.out.println("Breed: " + pack[3].breed);
		System.out.println("Number of legs: " + pack[3].numberOfLegs);
		System.out.println("");
	}
	
};