public class Dog{
	public String name;
	public String breed;
	public int numberOfLegs;
	public int counter = 0;
	
	public String canBark(){
		return name + " the dog can bark. it's breed is a " + breed;
	};
	
	public void canFetch(){
		if(numberOfLegs>0 && counter<5){
			System.out.println("This Dog has " + numberOfLegs + " legs. It can play fetch");
			System.out.println("");
			this.counter++;
		}else if (numberOfLegs<=0){
			System.out.println("This Dog has " + numberOfLegs + " legs. It can't play fetch :( ");
			System.out.println("");
		}else{
			System.out.println("Your dog looks exhausted, maybe next time.");
			System.out.println("");
		};
	};
};